const Discord = require('discord.js');
exports.run = async function (client, message, args) {

  const hexcols = [0xFFB6C1, 0x4C84C0, 0xAD1A2C, 0x20b046, 0xf2e807, 0xf207d1, 0xee8419];


  if(!message.guild.member(client.user).permissions.has('MANAGE_NICKNAMES')) {
    return message.channel.send('', {
      embed: {
        color: hexcols[~~(Math.random() * hexcols.length)],
        title: "Error",
        description: "I do not have the permission `MANAGE_NICKNAMES` to do this"
      }
    }).catch(console.error);
  }

  let nick = args.slice(1).join(' ');
  let user = message.mentions.users.first();
  let modlog = message.guild.channels.find('name', 'mod-log');
  if(!modlog) {
    return message.channel.send('', {
      embed: {
        color: hexcols[~~(Math.random() * hexcols.length)],
        title: "Error",
        description: "I cannot find a `mod-log` channel!"
      }
    }).catch(console.error);
  }
  
  
  if(!user) {
    return message.channel.send('', {
      embed: {
        color: hexcols[~~(Math.random() * hexcols.length)],
        title: "Error",
        description: "You must mention a user to change their nick!"
      }
    }).catch(console.error);
  }
  
  
  if(nick.length < 1) {
    return message.channel.send('', {
      embed: {
        color: hexcols[~~(Math.random() * hexcols.length)],
        title: "Error",
        description: "You must specify a nickname!"
      }
    }).catch(console.error);
  }
  
  if(message.mentions.users.size > 1) {
    return message.channel.send('', {
      embed: {
        color: hexcols[~~(Math.random() * hexcols.length)],
        title: "Error",
        description: "You must mention a user to change their nick!"
      }
    }).catch(console.error);
  }

 
  message.guild.fetchMember(user).then(m => m.setNickname(nick))
    message.channel.send('', {
      embed: {
        color: hexcols[~~(Math.random() * hexcols.length)],
        title: "Nickname change",
        description: `The user ${user.username}#${user.discriminator}'s nick has been change:\nNick: ${nick}`
      }
    }).catch(console.error);




  const embed = new Discord.RichEmbed()
  .setColor(hexcols[~~(Math.random() * hexcols.length)])
  .setTimestamp()
  .addField('Action:', 'Nick change')
  .addField('User:', `${user.username}#${user.discriminator}`)
  .addField('nick:', `${nick}`)
  .addField('Moderator:', `${message.author.username}#${message.author.discriminator}`);

  return client.channels.get(modlog.id).send({ embed: embed });
};

exports.settings = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permlevel: 2
};

exports.about = {
  name: 'nick',
  description: 'Nicks a user',
  usage: 'nick <Mention> <Args>'
};
