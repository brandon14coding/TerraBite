const config = require('../config.json');

const moment = require('moment');

exports.getUserVisibleGuilds = async (client, userID) => {
    let websiteInfo = [];
    const guilds = client.guilds.array();
    for (const guild of guilds) {
        let member;
        if (guild.members.has(userID)) {
            member = guild.members.get(userID);
        } else {
            try {
                member = await guild.fetchMember(userID);
            } catch (err) {
                continue;
            }
        }
        let superMaintainer = config.developers.indexOf(userID) > -1 || config.owners.indexOf(userID) > -1;
        if (member.permissions.has("MANAGE_GUILD")/* || superMaintainer*/) {
            let guildObj = {
                id: guild.id,
                name: guild.name.replaceAll("'", " "),
                members: guild.memberCount,
                created: guild.createdAt.toDateString(),
                icon: guild.icon,
                iconURL: guild.iconURL || config.host + '/img/discord-icon.png',
                region: guild.region,
                rawCreated: moment(guild.createdAt),
                formattedCreated: Math.ceil((Date.now() - guild.createdAt) / 86400000),
                ownerAvatar: guild.owner.user.displayAvatarURL,
                ownerName: guild.owner.displayName,
                ownerUsername: guild.owner.user.tag
            };
            websiteInfo.push(guildObj);
        }
    }
    return websiteInfo;
};

String.prototype.replaceAll = function (search, replacement) {
    let target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};